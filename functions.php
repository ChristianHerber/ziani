<?php
	register_nav_menu( 'menu_topo', 'ziani' );
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'blog', '348', '163', true );
	add_image_size( 'sobre', '969', '275', true );
	add_image_size( 'video', '128', '101', true );
	add_image_size( 'servico', '468', '468', true );