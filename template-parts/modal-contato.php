	<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
			<form class="form-contato" id="form-contato" method="post">
			  <div class="form-row">
			    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
			      <input type="text" name="nome" class="form-control form-control-lg" id="name" placeholder="Nome">
			    </div>
			    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
			      <input type="email" name="email" class="form-control form-control-lg" id="email" placeholder="E-mail" required="required">
			    </div>
			    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
			      <input type="text" name="telefone" class="form-control form-control-lg" id="telefone" placeholder="Telefone">
			    </div>
			  </div>
			  <div class="form-row">
			    <div class="form-group col-12">
			      <textarea name="msg" class="form-control form-control-lg" id="msg" placeholder="Digite aqui sua mensagem..." required="required"></textarea>
			    </div>
			  </div>
			  <input type="hidden" id="assunto" name="assunto" value="">
			  <div class="form-row">
			    <div class="form-group col-12">
			  	  <button type="submit" class="form-control btn btn-lg btn-success"><i class="fa fa-send fa-lg"></i> Enviar</button>
			    </div>
			  </div>
			</form>
	      </div>
	    </div>
	  </div>
	</div>