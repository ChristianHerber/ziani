	<div class="container-fluid bloco" id="blog">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center"><h2>Blog</h2></div>

		        <?php
		            global $postBlog;
		            $argsBlog = array ( 'post_type' => 'post', 'post_per_page' => 3, 'numberposts' => 3 );
		            $mypostsBlog = get_posts ($argsBlog);
		            foreach ( $mypostsBlog as $postBlog ): setup_postdata($postBlog);
		            $image_idBlog = get_post_thumbnail_id($postBlog->ID);
		            $image_urlBlog = wp_get_attachment_image_src($image_idBlog, 'blog');
		            $imageBlogDestaque = $image_urlBlog[0];
		            $tituloBlog = $postBlog->post_title;
		            $contentBlog = $postBlog->post_content;
		            $linkBlog = get_the_permalink($postBlog->ID);

		        ?>

				<div class="col-lx-4 col-lg-4 col-md-4 col-sm-12 col-12 mb-2" itemscope itemtype="http://schema.org/NewsArticle">
					<div class="card">
					 <a href="<?php echo $linkBlog; ?>">
					  <img class="card-img-top" src="<?php echo $imageBlogDestaque; ?>" alt="" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
					  <div class="card-body">
					    <h4 class="card-title" itemprop="name"><?php echo $tituloBlog; ?></h4>
					    <h6>14/11/2017</h6>
					    <p class="card-text" itemprop="description"><?php echo substr($contentBlog, 0, 95); ?>...</p>
					  </div>
					 </a>
					</div>
				</div>

			<?php endforeach; ?>

				<div class="col-12 text-center m-30">
					<a href="category/blog" class="btn btn-success btn-lg">Ver mais <i class="fa fa-plus-circle"></i></a>
				</div>

			</div>
		</div>
	</div>