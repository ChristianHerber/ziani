	<div class="container-fluid bloco" id="produto">
		<div class="container">
			<div class="row">

				<div class="col-12 text-center"><h2>Produtos e Serviços</h2></div>

		        <?php
		            $cont=3;
		            global $postProdServ;
		            $argsProdServ = array ( 'post_type'=>'produto-e-servico', 'post_per_page'=>4, 'numberposts'=>4 );
		            $mypostsProdServ = get_posts ($argsProdServ);
		            foreach ( $mypostsProdServ as $postProdServ ): setup_postdata($postProdServ);
		            $image_idProdServ = get_post_thumbnail_id($postProdServ->ID);
		            $image_urlProdServ = wp_get_attachment_image_src($image_idProdServ, 'servico');
		            $imageProdServ = $image_urlProdServ[0];
		            $tituloProdServ = $postProdServ->post_title;
		            $contentProdServ = $postProdServ->post_content;
		            $linkProdServ = get_the_permalink($postProdServ);
		            $customFieldServ = get_post_custom($postProdServ->ID);
		        ?>
				

				<div class="col-12" itemscope itemtype="http://schema.org/IndividualProduct">
					<div class="card flex-md-row mb-4 box-shadow h-md-250">
						<img class="card-img-right flex-auto d-none d-md-block <?php $resto = $cont%2; if( $resto == 0 ){ echo 'sr-only'; } ?>" src="<?php echo $imageProdServ; ?>">
						<div class="card-body d-flex flex-column align-items-start">
							<h3 class="card-title" itemprop="name"><?php echo $tituloProdServ; ?></h3>
							<span class="card-text mb-auto" itemprop="description">
								<?php echo $customFieldServ['wpcf-descricao'][0]; ?>
								<?php //echo substr($contentProdServ, 0, 255); ?>
							</span>
							<a href="<?php echo $linkProdServ; ?>" class="btn btn-success btn-lg">Saiba mais</a>
						</div>
						<img class="card-img-right flex-auto d-none d-md-block <?php $resto = $cont%2; if( $resto !== 0 ){ echo 'sr-only'; } ?>" src="<?php echo $imageProdServ; ?>">
					</div>
				</div>

				<?php $cont++; endforeach; ?>

			</div>
		</div>
	</div>