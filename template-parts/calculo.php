	<div class="container-fluid bloco" id="calculo">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center"><h2>Planeje</h2></div>
				<div class="col-12 text-center">
					<p><h5>Cálculo de mudas por Hectare</h5></p>
					<form class="mb-3 text-left">
						<div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<label for="hectare">Área em Hectares</label>
							<input type="number" min="0" class="form-control" id="hectare" required="required">
						</div>
						<div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<label for="metro-linha">Metros entre as linhas</label>
							<input type="number" min="0" class="form-control" id="metro-linha" required="required">
						</div>
						<div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<label for="metro-muda">Metros entre as mudas</label>
							<input type="number" min="0" class="form-control" id="metro-muda" required="required">
						</div>
						<div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<button class="btn btn-success btn-lg w-100" id="calcular"><i class="fa fa-calculator"></i> Calcular</button>
						</div>
					</form>
					<div class="col-12 text-center" id="resultado-calculo"></div>
				</div>
			</div>
		</div>
	</div>