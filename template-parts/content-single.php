	<div class="container-fluid bloco" id="sobre">
		<div class="container">
			<div class="row">
				<?php
					if ( have_posts() ) : while ( have_posts() ) : the_post();
					$customVideo = get_post_custom( $post->ID );
				?>

				<div class="col-12 text-center "><h2><?php the_title(); ?></h2></div>

				<div class="col-12 text-justify <?php if(is_singular( $post_types = 'videos' )){ echo ' embed-responsive embed-responsive-16by9'; } ?>">
					<?php
						if (is_singular( $post_types = 'videos' )) {
							echo $customVideo['wpcf-video'][0]; 
						} else {
							the_content();
						}
					?>
				</div>

				<?php endwhile; ?>
				<?php else: ?>
				<?php endif; ?> 
			</div>
		</div>
	</div>