	<div class="container-fluid bloco" id="video">
		<div class="container">
			<div class="row">

				<div class="col-12 text-center"><h2>Vídeos</h2></div>

				<div class="col-lx-7 col-lg-7 col-md-12 col-sm-12 col-12 embed-responsive embed-responsive-16by9 video-player">
					<iframe embed-responsive-item width="560" height="315" src="" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
				</div>
				<div class="col-lx-5 col-lg-5 col-md-12 col-sm-12 col-12 no-gutters">

			        <?php
			            $i=1;
			            global $postVideo;
			            $argsVideo = array ( 'post_type'=>'videos', 'post_per_page'=>4, 'numberposts'=>4 );
			            $mypostsVideo = get_posts ($argsVideo);
			            foreach ( $mypostsVideo as $postVideo ): setup_postdata($postVideo);
			            $image_idVideo = get_post_thumbnail_id($postVideo->ID);
			            $image_urlVideo = wp_get_attachment_image_src($image_idVideo, 'video');
			            $imageVideoDestaque = $image_urlVideo[0];
			            $tituloVideo = $postVideo->post_title;
			            $contentVideo = $postVideo->post_content;
			            $customVideo = get_post_custom( $postVideo->ID );
			            $dataVideo = get_the_date( 'd/m/Y', $postVideo->ID );
			        ?>

					<div itemscope itemtype="http://schema.org/VideoObject" class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 video-item" iframe='<?php echo $customVideo['wpcf-video'][0]; ?>'>
						<div class="card flex-md-row mb-2 box-shadow h-md-250">
							<img src="<?php echo $imageVideoDestaque; ?>" class="">
							<div class="card-body d-flex flex-column align-items-start">
								<h3 class="mb-0">
									<a class="text-dark" href="#" itemprop="name"><?php echo $tituloVideo; ?></a>
								</h3>
								<div class="mb-1 text-muted"><?php echo $dataVideo; ?></div>
							</div>
						</div>
					</div>

				<?php endforeach; ?>

				</div>

				<div class="col-12 text-center m-30">
					<a href="arquivo-de-video/video" class="btn btn-success btn-lg">Ver mais <i class="fa fa-plus-circle"></i></a>
				</div>

			</div>
		</div>
	</div>