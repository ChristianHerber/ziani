	<a href="/wp-admin"><i class="fa fa-unlock-alt"></i></a>

	<div class="container-fluid" id="topo">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center logo">
					<img src="<?php bloginfo( 'template_url' ); ?>/img/logo-site.png" alt="Ziani Florestal" class="img-fluid mt-20p">
				</div>
				<div class="col-12 text-center btn-link">
					<p><a href="#sobre" class="img-link"><i class="fa fa-arrow-circle-down fa-lg fa-3x"></i></a></p>
				</div>
			</div>
		</div>
	</div>