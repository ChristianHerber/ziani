	<div class="container-fluid bloco" id="sobre">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center"><h2>Sobre</h2></div>
				<div class="col-12 text-justify">
			    <?php
			        $pageId = 17;
			        $id = get_post($pageId);

			        echo wpautop(substr($id->post_content, 0, 697));

		            $image_idSobre = get_post_thumbnail_id($id);
		            $image_urlSobre = wp_get_attachment_image_src($image_idSobre, 'sobre');
		            $imagePageSobre = $image_urlSobre[0];
			    ?>
				</div>
				<div class="col-12">
					<img src="<?php echo $imagePageSobre; ?>" alt="Ziani Florestal" class="img-fluid w-100">
				</div>
				<div class="col-12 text-center m-30">
					<a href="/sobre" class="btn btn-success btn-lg">Ver mais <i class="fa fa-plus-circle"></i></a>
				</div>
			</div>
		</div>
	</div>