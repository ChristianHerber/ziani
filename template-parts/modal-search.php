	<div class="modal fade" id="modalPesquisar" tabindex="-1" role="dialog" aria-labelledby="modalPesquisar" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
			<form action="/">
			  <div class="form-row">
			    <div class="form-group col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12">
			      <input type="text" name="s" class="form-control form-control-lg" id="s" placeholder="Pesquiar...">
			    </div>
			    <div class="form-group col-xl-2 col-lg-2 col-md-12 col-sm-12 col-12">
			  	  <button type="submit" class="form-control btn btn-lg btn-success"><i class="fa fa-search fa-lg"></i></button>
			    </div>
			  </div>
			</form>
	      </div>
	    </div>
	  </div>
	</div>