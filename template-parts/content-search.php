	<div class="container-fluid bloco" id="blog">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<h2>Resultados da Pesquisa</h2>
				</div>

			</div>

			<div class="row" id="wrap-selector">

				<?php
					if ( have_posts() ) : while ( have_posts() ) : the_post();
				    $image_id = get_post_thumbnail_id($post->ID);
				    $image_url = wp_get_attachment_image_src($image_id, 'blog');
				    $image = $image_url[0];
				    $tituloCategory = $post->post_title;
				    $resumoCategory = get_the_excerpt($post->ID);
				    $linkCategory = get_the_permalink($post->ID);
				    $dateCategory = get_the_date( 'd/m/Y', $post->ID );
				?>
				
				<div class="col-lx-4 col-lg-4 col-md-4 col-sm-12 col-12 mb-2">
					<div class="card">
					 <a href="<?php echo $linkCategory; ?>">
					  <img class="card-img-top" src="<?php echo $image; ?>" alt="<?php echo $tituloCategory; ?>">
					  <div class="card-body">
					    <h4 class="card-title"><?php echo $tituloCategory; ?></h4>
					    <h6><?php echo $dateCategory; ?></h6>
					    <p class="card-text"><?php echo $resumoCategory; ?></p>
					  </div>
					 </a>
					</div>
				</div>

				<?php endwhile; ?>
				<?php else: ?>
				<?php endif; ?>


				<div class="col-12 text-center m-30">
					<p><?php the_posts_pagination( array( '' ) ); ?></p>
				</div>

			</div>
		</div>
	</div>