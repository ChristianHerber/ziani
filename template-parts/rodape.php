	<div class="container-fluid bloco" id="rodape">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<button class="btn btn-success-inverse btn-lg mb-2 btn-email" data-toggle="modal" data-target="#exampleModalCenter" assunto="email">Enviar Email</button>
				</div>
				<div class="col-lx-6 col-lg-6 col-md-6 col-md-12 col-sm-12 col-12 col-dir">
					<p><i class="fa fa-phone fa-lg"></i> (65) 3325-0407</p>
					<p><i class="fa fa-mobile fa-lg"></i> <i class="fa fa-whatsapp fa-lg"></i> (65) 9 9925-0407</p>
					<p><i class="fa fa-envelope fa-lg"></i> contato@zianiflorestal.com</p>
					<p><i class="fa fa-map-marker fa-lg"></i> Rod. MT 358 Km 167,8 - Zona Rural, Tangará da Serra - MT</p>
					<p><i class="fa fa-clock-o fa-lg"></i> Seg à Sex das 07h - 17h</p>
					<p><i class="fa fa-facebook fa-lg"></i> @ZianiFlorestal1</p>
				</div>
				<div class="col-lx-6 col-lg-6 col-md-6 col-md-12 col-sm-12 col-12 col-face">
					<div class="fb-page" data-height="250" data-href="https://www.facebook.com/ZianiFlorestal1/" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false"><blockquote cite="https://www.facebook.com/ZianiFlorestal1/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/ZianiFlorestal1/">Ziani Mudas Florestais</a></blockquote></div>
				</div>
			</div>
		</div>
	</div>