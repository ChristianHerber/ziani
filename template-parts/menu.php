	<nav class="navbar sticky-top navbar-expand-lg">
		<div class="container">
			<a class="navbar-brand img-link" href="http://zianiflorestal.com#topo"><img src="<?php bloginfo( 'template_url' ); ?>/img/logo-menu.png"></a>
			<button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"><i class="fa fa-bars fa-lg fa-2x"></i></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav mr-auto navigation text-center">
					<?php if (function_exists('getNavMenu')): ?>
						<?php echo getNavMenu('menu_topo','hover'); ?>
					<?php endif; ?>
					<li class="nav-item">
						<a class="nav-link" href="#" data-toggle="modal" data-target="#modalPesquisar"><i class="fa fa-search fa-lg"></i></a>
					</li>
					<li class="nav-item" id="no-prevent">
						<a class="nav-link" href="https://www.facebook.com/ZianiFlorestal1/" target="_blank"><i class="fa fa-facebook fa-lg"></i></a>
					</li>
				</ul>
			</div>
		</div>
	</nav>