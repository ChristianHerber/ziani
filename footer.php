</body>
	<script src="<?php bloginfo( 'template_url' ); ?>/js/jquery.min.js"></script>
	<script src="<?php bloginfo( 'template_url' ); ?>/js/bootstrap.min.js"></script>
	<script src="<?php bloginfo( 'template_url' ); ?>/js/popper.min.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script src="<?php bloginfo( 'template_url' ); ?>/js/scripts.js"></script>
	<?php wp_footer(); ?>
</html>