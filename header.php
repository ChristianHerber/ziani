<?php header("Access-Control-Allow-Origin: *"); ?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<title>Ziani Florestal</title>
	<meta name="description" content="">
	<meta name="keywords" content="ziani, florestal, tangara, mudas, eucalipto, mudas nativas, mudas ornamentais, lenhas, mudas de eucalipto">
	<meta name="author" content="Uebi! Agência Digital">
	<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/style.css">
	<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/fontawesome.min.css">
	<link rel="icon" href="<?php bloginfo( 'template_url' ); ?>/img/favicon.png">
	<?php wp_head(); ?>
</head>
<body>
	
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.11&appId=1442147942707338&autoLogAppEvents=1';
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>	